https://gmplib.org/download/gmp/gmp-6.3.0.tar.xz
https://www.mpfr.org/mpfr-current/mpfr-4.2.1.tar.xz
https://ftp.gnu.org/gnu/mpc/mpc-1.3.1.tar.gz
https://libisl.sourceforge.io/isl-0.26.tar.xz
https://ftp.gnu.org/gnu/gcc/gcc-13.2.0/gcc-13.2.0.tar.xz