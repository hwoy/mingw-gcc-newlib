#!/bin/sh
source ../0_append_distro_path_32.sh

SNAME=gcc
SVERSION=13.2.0


# Extract vanilla sources.

decompress()
{
	untar_file gmp-6.3.0.tar.xz
	untar_file mpfr-4.2.1.tar.xz
	untar_file mpc-1.3.1.tar.gz
	untar_file isl-0.26.tar.xz

	untar_file ${SNAME}-${SVERSION}.tar.xz
}

prepare()
{
	cd patch
	
	patch -d ${X_BUILDDIR}/gmp-6.3.0 -p2 < do-not-use-dllimport.diff
	patch -d ${X_BUILDDIR}/gmp-6.3.0 -p1 < gmp-staticlib.diff
	
	patch -d ${X_BUILDDIR}/isl-0.26 -p1 < isl-0.14.1-no-undefined.patch
	autoreconf -fi ${X_BUILDDIR}/isl-0.26

	del_file_exists ${X_BUILDDIR}/${SNAME}-${SVERSION}/intl/relocatex.c ${X_BUILDDIR}/${SNAME}-${SVERSION}/intl/relocatex.h

  apply_patch_p1 \
    0002-Relocate-libintl.patch \
    0003-Windows-Follow-Posix-dir-exists-semantics-more-close.patch \
    0005-Windows-Don-t-ignore-native-system-header-dir.patch \
    0006-Windows-New-feature-to-allow-overriding.patch \
    0007-Build-EXTRA_GNATTOOLS-for-Ada.patch \
    0008-Prettify-linking-no-undefined.patch \
    0011-Enable-shared-gnat-implib.patch \
    0012-Handle-spaces-in-path-for-default-manifest.patch \
    0014-gcc-9-branch-clone_function_name_1-Retain-any-stdcall-suffix.patch \
    0020-libgomp-Don-t-hard-code-MS-printf-attributes.patch \
    0021-PR14940-Allow-a-PCH-to-be-mapped-to-a-different-addr.patch
	
  # Enable diagnostic color under mintty
  # based on https://github.com/BurntSushi/ripgrep/issues/94#issuecomment-261761687
  apply_patch_p1 \
    0140-gcc-diagnostic-color.patch

  # workaround for AVX misalignment issue for pass-by-value arguments
  #   cf. https://github.com/msys2/MSYS2-packages/issues/1209
  #   cf. https://sourceforge.net/p/mingw-w64/discussion/723797/thread/bc936130/ 
  #  Issue is longstanding upstream at https://gcc.gnu.org/bugzilla/show_bug.cgi?id=54412
  #  Potential alternative: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=939559
  # https://github.com/msys2/MINGW-packages/pull/8317#issuecomment-824548411
  apply_patch_p1 \
    0200-add-m-no-align-vector-insn-option-for-i386.patch

  # Patch from https://gcc.gnu.org/pipermail/gcc-patches/2022-January/588341.html
  # Related bug: https://gcc.gnu.org/bugzilla/show_bug.cgi?id=95130
  apply_patch_p1 \
    0300-override-builtin-printf-format.patch

  # https://salsa.debian.org/toolchain-team/gcc/-/blob/b953a3f9/debian/patches/rust-enabled.diff
  apply_patch_p1 \
    2000-enable-rust.patch

  apply_patch_p1 \
    2001-fix-building-rust-on-mingw-w64.patch

  # backport: https://github.com/msys2/MINGW-packages/issues/17599
  # https://inbox.sourceware.org/gcc-patches/a22433f5-b4d2-19b7-86a2-31e2ee45fb61@martin.st/T/
  apply_patch_p1 \
    2f7e7bfa3c6327793cdcdcb5c770b93cecd49bd0.patch \
    3eeb4801d6f45f6250fc77a6d3ab4e0115f8cfdd.patch

  # https://gcc.gnu.org/bugzilla/show_bug.cgi?id=110315#c7
  apply_patch_p1 \
    777aa930b106fea2dd6ed9fe22b42a2717f1472d.patch

	cd ..
	
	cd ${X_BUILDDIR}/${SNAME}-${SVERSION}
	
  # do not expect ${prefix}/mingw symlink - this should be superceded by
  # 0005-Windows-Don-t-ignore-native-system-header-dir.patch .. but isn't!
  sed -i 's/${prefix}\/mingw\//${prefix}\//g' configure

  # change hardcoded /mingw prefix to the real prefix .. isn't this rubbish?
  # it might work at build time and could be important there but beyond that?!
  local MINGW_NATIVE_PREFIX=$(cygpath -am ${MINGW_PREFIX})
  sed -i "s#\\/mingw\\/#${MINGW_NATIVE_PREFIX//\//\\/}\\/#g" gcc/config/i386/mingw32.h

	#sed -i 's/#define abort()/\/\/#define abort()/' ${X_BUILDDIR}/${SNAME}-${SVERSION}/gcc/system.h
}

build()
{
	#No use libiconv
	#export am_cv_func_iconv=no
	export lt_cv_deplibs_check_method='pass_all'
	#export gcc_cv_libc_provides_ssp=yes

	cd ${X_BUILDDIR}

	# Prepare to build gcc.
	mv ${SNAME}-${SVERSION} src
	mv gmp-6.3.0 src/gmp
	mv mpfr-4.2.1 src/mpfr
	mv mpc-1.3.1 src/mpc
	mv isl-0.26 src/isl


	# Configure.
	GCC_PARAMS=" "

	_config=""
	_config="${_config} --with-gnu-as --with-gnu-ld"
	#_config="${_config} --disable-tm-clone-registry"
	#_config="${_config} --disable-tls"
	#_config="${_config} --disable-libffi"
	#_config="${_config} --disable-decimal-float"
	#_config="${_config} --enable-gnu-indirect_function"
	#_config="${_config} --with-libelf"
	#_config="${_config} --enable-gnu-indirect_function"
	#_config="${_config} --with-multilib-list=rmprofile"

	mkdir build
	cd build

	../src/configure --enable-languages=c \
		--build=${X_BUILD} --host=${X_HOST} --target=${X_TARGET} \
		--prefix=${NEW_DISTRO_ROOT} \
		--with-sysroot=${NEW_DISTRO_ROOT} \
		--with-build-sysroot=${NEW_DISTRO_ROOT} \
		--disable-win32-registry \
		--disable-bootstrap \
        --disable-nls \
        --disable-libssp \
        --disable-shared \
        --disable-threads \
        --disable-libgomp \
        --disable-werror \
        --disable-lto \
        --disable-libffi \
        --disable-decimal-float \
        --disable-libmudflap \
        --disable-libquadmath \
        --disable-libstdcxx-pch \
        --disable-libsanitizer \
        --disable-tls \
        --enable-multilib \
        --enable-interwork \
        --without-headers \
        --with-libelf \
        --with-newlib \
		--with-pkgversion="${PROJECTNAME} ${REV}, Built by ${AUTHOR}" \
		${_config} \
		${GCC_PARAMS}


	# --enable-languages=c,c++        : I want C and C++ only.
	# --build=${X_BUILD}      : I want a native compiler.
	# --host=${X_HOST}       : Ditto.
	# --target=${X_TARGET}     : Ditto.
	# --disable-multilib              : I want 64-bit only.
	# --prefix=${X_BUILDDIR}/dest       : I want the compiler to be installed here.
	# --with-sysroot=${X_BUILDDIR}/dest : Ditto. (This one is important!)
	# --disable-libstdcxx-pch         : I don't use this, and it takes up a ton of space.
	# --disable-libstdcxx-verbose     : Reduce generated executable size. This doesn't affect the ABI.
	# --disable-nls                   : I don't want Native Language Support.
	# --disable-shared                : I don't want DLLs.
	# --disable-win32-registry        : I don't want this abomination.
	# --enable-threads=posix          : Use winpthreads.
	# --enable-libgomp                : Enable OpenMP.
	# --with-zstd=$X_DISTRO_ROOT      : zstd is needed for LTO bytecode compression.
	# --disable-bootstrap             : Significantly accelerate the build, and work around bootstrap comparison failures.
	
	
	#--disable-docs \
    #--disable-gcov \
    #--disable-libffi \
    #--disable-libgomp \
    #--disable-libmudflap \
    #--disable-libquadmath \
    #--disable-libstdcxx-pch \

	# Build and install.
	
	# make $X_MAKE_JOBS V=1 all
	# DESTDIR=${X_BUILDDIR} make install
	make all-gcc -j16
	make all-target-libgcc -j16
	#make all-target-libstdc++-v3 -j16
	DESTDIR=${X_BUILDDIR}  make install-gcc
	DESTDIR=${X_BUILDDIR}  make install-target-libgcc
	#DESTDIR=${X_BUILDDIR}  make install-target-libstdc++-v3

	# Cleanup.
	cd ${X_BUILDDIR}
	rm -rf build src
	mv c ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}
	cd ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}

	#rm -rf usr
	#cp libgcc_s

	find -name "*.la" -type f -print -exec rm {} ";"
	find -name "*.exe" -type f -print -exec strip -s {} ";"

	zip7 ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}.7z

}


decompress

prepare

build
