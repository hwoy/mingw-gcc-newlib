# Reject expansion of unset variables.
set -u

# Exit when a command fails.
if [ "${PS1:-}" == "" ]; then set -e; fi

source ../0_common_head.sh
PROJECTNAME=${PROJ}64-${_default_msvcrt}
export NEW_DISTRO_ROOT=/c/${PROJECTNAME}

export X_BUILD=x86_64-w64-mingw32
export X_HOST=${X_BUILD}

#export X_DISTRO_ROOT=${NEW_DISTRO_ROOT}

export LDFLAGS=" "
export CFLAGS=" -O2"
export CXXFLAGS=" -O2"

source ../0_common.sh

# Print commands.
if [ "${PS1:-}" == "" ]; then set -x; fi
