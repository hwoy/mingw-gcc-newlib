PROJ=cumMinGW
REV=Latest
AUTHOR="Watthanachai Dueanklang"

#_languages="c,lto,c++,fortran"
_languages="c,lto"
_default_msvcrt=nolibc
#_default_msvcrt=ucrt
X_THREAD=threads

export X_TARGET=i386-unknown-elf
#export X_DISTRO_ROOT=/i/msys64/mingw32
