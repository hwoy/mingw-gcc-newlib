#!/bin/sh
# https://sourceware.org/git/?p=binutils-gdb.git;a=commit;h=999566402e3d7c69032bbf47e28b44fc0926fe62

source ../0_append_distro_path.sh

SNAME=binutils
SVERSION=2.41

decompress()
{
	untar_file ${SNAME}-${SVERSION}.tar.xz
}

prepare()
{
	cd patch

	apply_patch_p1 \
	0002-check-for-unusual-file-harder.patch \
    0010-bfd-Increase-_bfd_coff_max_nscns-to-65279.patch \
    0110-binutils-mingw-gnu-print.patch
	
  # Add an option to change default bases back below 4GB to ease transition
  # https://github.com/msys2/MINGW-packages/issues/7027
  # https://github.com/msys2/MINGW-packages/issues/7023
  apply_patch_p1 2001-ld-option-to-move-default-bases-under-4GB.patch
  
  # https://github.com/msys2/MINGW-packages/pull/9233#issuecomment-889439433
  apply_reverse_patch_p1  2003-Restore-old-behaviour-of-windres-so-that-options-con.patch

	# patches for reproducibility from Debian:
	# https://salsa.debian.org/mingw-w64-team/binutils-mingw-w64/-/tree/master/debian/patches
	apply_patch_p2 "reproducible-import-libraries.patch"
	apply_patch_p2 "specify-timestamp.patch"

  # Handle Windows nul device
  # https://github.com/msys2/MINGW-packages/issues/1840
  # https://github.com/msys2/MINGW-packages/issues/10520
  # https://github.com/msys2/MINGW-packages/issues/14725

  # https://gcc.gnu.org/bugzilla/show_bug.cgi?id=108276
  # https://gcc.gnu.org/pipermail/gcc-patches/2023-January/609487.html
  apply_patch_p1 libiberty-unlink-handle-windows-nul.patch
  

  apply_patch_p1 \
    6aadf8a04d162feb2afe3c41f5b36534d661d447.patch \
    398f1ddf5e89e066aeee242ea854dcbaa8eb9539.patch \
    26d0081b52dc482c59abba23ca495304e698ce4b.patch \
    8606b47e94078e77a53f3cd714272c853d2add22.patch \
    54d57acf610e5db2e70afa234fd4018207606774.patch

	cd ..
}

build()
{
	cd ${X_BUILDDIR}
	mv ${SNAME}-${SVERSION} src
	mkdir build
	cd build

	BINUTILS_PARAM=" --enable-64-bit-bfd"

	../src/configure \
		--build=${X_BUILD} \
		--host=${X_HOST} \
		--target=${X_TARGET} \
		--prefix=${NEW_DISTRO_ROOT} \
		--disable-multilib \
		--disable-shared \
		--enable-lto \
		--disable-werror \
		--disable-nls \
		--disable-rpath \
		${BINUTILS_PARAM}

	make $X_MAKE_JOBS all
	DESTDIR=${X_BUILDDIR} make  install

	cd ${X_BUILDDIR}
	rm -rf build src
	mv c ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}
	cd ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}
	rm -rf ${PROJECTNAME}/lib/*.la share
	# https://github.com/msys2/MINGW-packages/issues/7890
	rm -rf ${PROJECTNAME}/lib/bfd-plugins/libdep.a

	zip7 ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}.7z
}

decompress

prepare

build

