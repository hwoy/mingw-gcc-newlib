```
Distro name: cumMinGW
Created by: Watthanachai Dueanklang
Reversion: Latest
Compilers: c,lto
Thread: threads
Exception: SEH for 64 and Dwarf-2 for 32
Libc: nolibc
```


**====================== Packages ======================**
```
binutils-2.41.tar.xz
gmp-6.3.0.tar.xz
mpfr-4.2.1.tar.xz
mpc-1.3.1.tar.gz
isl-0.26.tar.xz
gcc-13.2.0.tar.xz
newlib-4.3.0.20230120.tar.gz
```
**====================== Packages ======================**
