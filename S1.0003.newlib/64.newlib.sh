#!/bin/sh
source ../0_append_distro_path.sh

SNAME=newlib
SVERSION=4.3.0.20230120


# Extract vanilla sources.

decompress()
{

	untar_file ${SNAME}-${SVERSION}.tar.gz
}

prepare()
{
	export PATH=${NEW_DISTRO_ROOT}/bin:$PATH
}

build()
{

	cd ${X_BUILDDIR}
	
	mv ${SNAME}-${SVERSION} src
	

	mkdir build
	cd build

	../src/configure \
		--build=${X_BUILD} --host=${X_HOST} --target=${X_TARGET} \
		--prefix=${NEW_DISTRO_ROOT} \
        --enable-newlib-io-long-long \
        --enable-newlib-io-c99-formats \
        --enable-newlib-register-fini \
        --enable-newlib-retargetable-locking \
        --disable-newlib-supplied-syscalls \
        --disable-nls


	# --enable-languages=c,c++        : I want C and C++ only.
	# --build=${X_BUILD}      : I want a native compiler.
	# --host=${X_HOST}       : Ditto.
	# --target=${X_TARGET}     : Ditto.
	# --disable-multilib              : I want 64-bit only.
	# --prefix=${X_BUILDDIR}/dest       : I want the compiler to be installed here.
	# --with-sysroot=${X_BUILDDIR}/dest : Ditto. (This one is important!)
	# --disable-libstdcxx-pch         : I don't use this, and it takes up a ton of space.
	# --disable-libstdcxx-verbose     : Reduce generated executable size. This doesn't affect the ABI.
	# --disable-nls                   : I don't want Native Language Support.
	# --disable-shared                : I don't want DLLs.
	# --disable-win32-registry        : I don't want this abomination.
	# --enable-threads=posix          : Use winpthreads.
	# --enable-libgomp                : Enable OpenMP.
	# --with-zstd=$X_DISTRO_ROOT      : zstd is needed for LTO bytecode compression.
	# --disable-bootstrap             : Significantly accelerate the build, and work around bootstrap comparison failures.
	
	
	#--disable-docs \
    #--disable-gcov \
    #--disable-libffi \
    #--disable-libgomp \
    #--disable-libmudflap \
    #--disable-libquadmath \
    #--disable-libstdcxx-pch \

	# Build and install.
	
	make $X_MAKE_JOBS all
	DESTDIR=${X_BUILDDIR} make install

	# Cleanup.
	cd ${X_BUILDDIR}
	rm -rf build src
	mv c ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}
	cd ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}

	#rm -rf usr
	#cp libgcc_s

	find -name "*.la" -type f -print -exec rm {} ";"
	find -name "*.exe" -type f -print -exec strip -s {} ";"

	zip7 ${SNAME}-${SVERSION}-${X_HOST}-${X_THREAD}-${_default_msvcrt}-${REV}.7z

}


decompress

prepare

build
